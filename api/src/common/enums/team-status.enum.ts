export enum TeamStatus {
    Pending,
    Accepted,
    Rejected
  }