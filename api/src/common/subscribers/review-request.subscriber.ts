import { Injectable, Inject } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, EntitySubscriberInterface, InsertEvent, UpdateEvent, Repository, RemoveEvent } from 'typeorm';
import { ActivityService } from '../../activity/activity.service';
import { ReviewRequest } from '../../data/entities/reviewRequest.entity';
import { Status } from '../enums/status.enum';

@Injectable()
export class ReviewRequestSubscriber implements EntitySubscriberInterface {
    constructor(
        @InjectConnection() readonly connection: Connection,
        private readonly activityService: ActivityService
    ) {
        connection.subscribers.push(this);
    }

    listenTo() {
        return ReviewRequest;
    }

    async afterInsert(event: InsertEvent<ReviewRequest>) {
        const user = await event.entity.assignee;
        const act = `${user.name} opened a review request with title: ${event.entity.title}`
        return await this.activityService.newActivity(act);

    }

    async afterUpdate(event: InsertEvent<ReviewRequest>) {
        if (event.entity !== undefined) {
            const status = await event.entity.status;
            const closed = await event.entity.isClosed;
            if (closed) {
                const act = `${event.entity.title} has been closed with status ${event.entity.status}`;
                this    .activityService.newActivity(act)
            } else {
                let act = '';
                switch (status) {
                    case Status.ChangeRequested:
                        act = `${event.entity.title} has Changes Requested`;
                        this.activityService.newActivity(act)
                        break;
                    case Status.UnderReview:
                        act = `${event.entity.title} is now Under Review`;
                        this.activityService.newActivity(act)
                        break;
                }
            }
        }

    };
}