import { Injectable, Inject } from '@nestjs/common';
import { InjectConnection, InjectRepository } from '@nestjs/typeorm';
import { Connection, EntitySubscriberInterface, InsertEvent, UpdateEvent, Repository, RemoveEvent } from 'typeorm';
import { Team } from '../../data/entities/team.entity';
import { ActivityService } from '../../activity/activity.service';

@Injectable()
export class TeamSubscriber implements EntitySubscriberInterface {
    constructor(
        @InjectConnection() readonly connection: Connection,
        private readonly activityService: ActivityService
    ) {
        connection.subscribers.push(this);
    }

    listenTo() {
        return Team;
    }

    async afterInsert(event: InsertEvent<Team>) {
        const user = await event.entity.author;
        const act = `${user.name} created team ${event.entity.name}`
        return await this.activityService.newActivity(act);
        
    }
}