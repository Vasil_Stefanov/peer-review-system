import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  JoinTable,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { Team } from './team.entity';
import { ReviewRequest } from './reviewRequest.entity';
import { Comment } from './comment.entity';
import { Notification } from './notification.entity';
import { TeamStatus } from '../../common/enums/team-status.enum';
import { Invites } from './invites.entity';

@Entity('users')
export class User {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @Column('nvarchar')
  password: string;

  @Column({default: 'user'})
  roles: string;

  @OneToMany(type => ReviewRequest, reviewRequest => reviewRequest.assignee)
  reviewRequest: Promise<ReviewRequest[]>;

  @ManyToMany(type => Team, team => team.users)
  @JoinTable()
  teams: Promise<Team[]>;

  @OneToMany(type => Invites, invites => invites.user)
  invites: Promise<Invites[]>

  @ManyToMany(type => ReviewRequest, reviewRequest => reviewRequest.users)
  @JoinTable()
  reviews: Promise<ReviewRequest[]>;

  @ManyToOne(type => ReviewRequest, reviewRequest => reviewRequest.voters)
  voted: Promise<ReviewRequest>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;

  @OneToMany(type => Notification, notifications => notifications.user)
  notification: Promise<Notification[]>;

  @OneToMany(type => Team, team => team.author)
  ofTeam: Promise<Team[]>;
}
