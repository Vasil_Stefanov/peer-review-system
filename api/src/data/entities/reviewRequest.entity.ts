import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    OneToMany,
    ManyToOne,
    ManyToMany,
} from 'typeorm';
import { User } from './user.entity';
import { Status } from '../../common/enums/status.enum';
import { Comment } from './comment.entity';
import { Team } from './team.entity';

@Entity('review-requests')
export class ReviewRequest {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    title: string;

    @Column('longtext')
    content: string;

    @Column()
    tags: string;

    @ManyToOne(type => User, user => user.reviewRequest)
    assignee: Promise<User>;

    @ManyToOne(type => Team, team => team.reviewRequest)
    team: Promise<Team>;

    @ManyToMany(type => User, user => user.reviews)
    users: Promise<User[]>;

    @OneToMany(type => Comment, comment => comment.reviewRequest)
    comments: Promise<Comment[]>;

    @OneToMany(type => User, user => user.voted)
    voters: Promise<User[]>

    @Column({default: ''})
    author: string;

    @Column({ type: 'enum', enum: Status })
    status: Status;

    @Column({ default: false })
    isClosed: boolean;
    
    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;
}