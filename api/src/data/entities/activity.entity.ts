import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
} from 'typeorm';

@Entity('activity')
export class Activity {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column()
    activity: string;

    @CreateDateColumn()
    dateCreated: Date;

    @Column({default: ''})
    user: string;
}