import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToMany,
    OneToMany,
    ManyToOne,
    PrimaryColumn,
} from 'typeorm';
import { User } from './user.entity';
import { ReviewRequest } from './reviewRequest.entity';
import { TeamStatus } from '../../common/enums/team-status.enum';
import { Team } from './team.entity';

@Entity('invites')
export class Invites {
    @PrimaryGeneratedColumn('uuid')
    id: string
    @Column({default: ''})
    team: string
    @ManyToOne(type => User, user => user.invites)
    user: Promise<User>
    @Column({ default: TeamStatus.Pending })
    status: TeamStatus
}