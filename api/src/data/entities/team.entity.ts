import {
  Entity,
  PrimaryGeneratedColumn,
  Column,
  CreateDateColumn,
  UpdateDateColumn,
  ManyToMany,
  OneToMany,
  ManyToOne,
} from 'typeorm';
import { User } from './user.entity';
import { ReviewRequest } from './reviewRequest.entity';
import { TeamStatus } from '../../common/enums/team-status.enum';
import { Invites } from './invites.entity';

@Entity('teams')
export class Team {
  @PrimaryGeneratedColumn('uuid')
  id: string;

  @Column('nvarchar')
  name: string;

  @ManyToMany(type => User, user => user.teams, {eager: true})
  users: Promise<User[]>;

  @OneToMany(type => ReviewRequest, reviewRequest => reviewRequest.team)
  reviewRequest: Promise<ReviewRequest[]>

  @ManyToOne(type => User, user => user.ofTeam)
    author: Promise<User>;

  @CreateDateColumn()
  createdOn: Date;

  @UpdateDateColumn()
  updatedOn: Date;
}
