import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
} from 'typeorm';
import { User } from './user.entity';

@Entity('notifications')
export class Notification {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @Column({default: ''})
    content: string;

    @ManyToOne(type => User, user => user.notification)
    user: Promise<User>;

    @Column({default: ''})
    key: string;
    
    @Column({default: false})
    checked: boolean;
}