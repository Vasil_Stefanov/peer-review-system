import {
    Entity,
    PrimaryGeneratedColumn,
    Column,
    CreateDateColumn,
    UpdateDateColumn,
    ManyToOne,
} from 'typeorm';
import { ReviewRequest } from './reviewRequest.entity';
import { User } from './user.entity';

@Entity('comment')
export class Comment {
    @PrimaryGeneratedColumn('uuid')
    id: string;

    @ManyToOne(type => ReviewRequest, reviewRequest => reviewRequest.comments)
    reviewRequest: Promise<ReviewRequest>;

    @Column({default: ''})
    author: string;

    @Column({default: ''})
    content: string;

    @CreateDateColumn()
    createdOn: Date;

    @UpdateDateColumn()
    updatedOn: Date;
}