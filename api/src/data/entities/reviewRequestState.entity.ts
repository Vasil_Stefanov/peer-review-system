import {
    Entity,
    Column,
    PrimaryColumn,
} from 'typeorm';

@Entity('review-requests-state')
export class ReviewRequestState {
    @PrimaryColumn({default: ''})
    reviewRequestId: string;

    @Column({default: 0})
    Accepted: number;

    @Column({default: 0})
    Rejected: number;

    @Column({default: 0})
    ChangeRequested: number;
}