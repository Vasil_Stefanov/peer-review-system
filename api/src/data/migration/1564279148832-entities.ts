import {MigrationInterface, QueryRunner} from "typeorm";

export class entities1564279148832 implements MigrationInterface {

    public async up(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `activity` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `teams` DROP FOREIGN KEY `FK_7ba8d427cf8f87440b02e672ac8`");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `authorId` `authorId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `notifications` DROP FOREIGN KEY `FK_692a909ee0fa9383e7859f9b406`");
        await queryRunner.query("ALTER TABLE `notifications` CHANGE `userId` `userId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `invites` DROP FOREIGN KEY `FK_f53061a24b71fb0f54cfb1629ae`");
        await queryRunner.query("ALTER TABLE `invites` CHANGE `userId` `userId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_361d1b63d6a3f2aa35ae386c9c5`");
        await queryRunner.query("ALTER TABLE `users` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `users` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `users` CHANGE `votedId` `votedId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `review-requests` DROP FOREIGN KEY `FK_51c0e40f3a0c68b08407c0d6af3`");
        await queryRunner.query("ALTER TABLE `review-requests` DROP FOREIGN KEY `FK_fa84437402414ab05266331e6cc`");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `assigneeId` `assigneeId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `teamId` `teamId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `comment` DROP FOREIGN KEY `FK_34fe0ca0b194dc25cce4d96197e`");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `reviewRequestId` `reviewRequestId` varchar(36) NULL");
        await queryRunner.query("ALTER TABLE `teams` ADD CONSTRAINT `FK_7ba8d427cf8f87440b02e672ac8` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `notifications` ADD CONSTRAINT `FK_692a909ee0fa9383e7859f9b406` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `invites` ADD CONSTRAINT `FK_f53061a24b71fb0f54cfb1629ae` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_361d1b63d6a3f2aa35ae386c9c5` FOREIGN KEY (`votedId`) REFERENCES `review-requests`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review-requests` ADD CONSTRAINT `FK_51c0e40f3a0c68b08407c0d6af3` FOREIGN KEY (`assigneeId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review-requests` ADD CONSTRAINT `FK_fa84437402414ab05266331e6cc` FOREIGN KEY (`teamId`) REFERENCES `teams`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `comment` ADD CONSTRAINT `FK_34fe0ca0b194dc25cce4d96197e` FOREIGN KEY (`reviewRequestId`) REFERENCES `review-requests`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
    }

    public async down(queryRunner: QueryRunner): Promise<any> {
        await queryRunner.query("ALTER TABLE `comment` DROP FOREIGN KEY `FK_34fe0ca0b194dc25cce4d96197e`");
        await queryRunner.query("ALTER TABLE `review-requests` DROP FOREIGN KEY `FK_fa84437402414ab05266331e6cc`");
        await queryRunner.query("ALTER TABLE `review-requests` DROP FOREIGN KEY `FK_51c0e40f3a0c68b08407c0d6af3`");
        await queryRunner.query("ALTER TABLE `users` DROP FOREIGN KEY `FK_361d1b63d6a3f2aa35ae386c9c5`");
        await queryRunner.query("ALTER TABLE `invites` DROP FOREIGN KEY `FK_f53061a24b71fb0f54cfb1629ae`");
        await queryRunner.query("ALTER TABLE `notifications` DROP FOREIGN KEY `FK_692a909ee0fa9383e7859f9b406`");
        await queryRunner.query("ALTER TABLE `teams` DROP FOREIGN KEY `FK_7ba8d427cf8f87440b02e672ac8`");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `reviewRequestId` `reviewRequestId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `comment` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `comment` ADD CONSTRAINT `FK_34fe0ca0b194dc25cce4d96197e` FOREIGN KEY (`reviewRequestId`) REFERENCES `review-requests`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `teamId` `teamId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `assigneeId` `assigneeId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `review-requests` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `review-requests` ADD CONSTRAINT `FK_fa84437402414ab05266331e6cc` FOREIGN KEY (`teamId`) REFERENCES `teams`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `review-requests` ADD CONSTRAINT `FK_51c0e40f3a0c68b08407c0d6af3` FOREIGN KEY (`assigneeId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `users` CHANGE `votedId` `votedId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `users` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `users` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `users` ADD CONSTRAINT `FK_361d1b63d6a3f2aa35ae386c9c5` FOREIGN KEY (`votedId`) REFERENCES `review-requests`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `invites` CHANGE `userId` `userId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `invites` ADD CONSTRAINT `FK_f53061a24b71fb0f54cfb1629ae` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `notifications` CHANGE `userId` `userId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `notifications` ADD CONSTRAINT `FK_692a909ee0fa9383e7859f9b406` FOREIGN KEY (`userId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `authorId` `authorId` varchar(36) NULL DEFAULT 'NULL'");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `updatedOn` `updatedOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `teams` CHANGE `createdOn` `createdOn` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
        await queryRunner.query("ALTER TABLE `teams` ADD CONSTRAINT `FK_7ba8d427cf8f87440b02e672ac8` FOREIGN KEY (`authorId`) REFERENCES `users`(`id`) ON DELETE NO ACTION ON UPDATE NO ACTION");
        await queryRunner.query("ALTER TABLE `activity` CHANGE `dateCreated` `dateCreated` datetime(6) NOT NULL DEFAULT 'current_timestamp(6)'");
    }

}
