import { IsEmail, IsString, IsArray } from 'class-validator';
import { User } from '../../data/entities/user.entity';

export class ShowReviewRequestDTO {
    @IsString()
    name: string;

    @IsString()
    team: string;
    
    @IsArray()
    reviewers: string[];

    @IsString()
    title: string;

    @IsString()
    content: string;

    @IsString()
    tags: string[];
}