import { IsString, IsArray } from 'class-validator';
import { User } from '../../data/entities/user.entity';

export class CreateReviewRequestDTO {
    @IsArray()
    reviewers: string[];

    @IsString()
    team: string;
    
    @IsString()
    title: string;

    @IsString()
    content: string;

    @IsArray()
    tags: string[];
}