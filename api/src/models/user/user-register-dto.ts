import { IsString } from 'class-validator';

export class UserRegisterDTO {
  @IsString()
  name: string;

  @IsString()
  password: string;
}
