import { IsString } from 'class-validator';
import { CreateReviewRequestDTO } from '../review-request/create-review-request-dto';
import { Team } from '../../data/entities/team.entity';
import { Invites } from '../../data/entities/invites.entity';
import { ReviewRequest } from '../../data/entities/reviewRequest.entity';
import { Notification } from '../../data/entities/notification.entity';

export class UserDTO {
    name: string;
    password: string;
    roles: string;
    reviewRequest: ReviewRequest[];
    teams: Team;
    invites: Invites[]
    reviews: ReviewRequest[]
    voted: ReviewRequest;
    notification: Notification[];
    ofTeam: Team[]
}