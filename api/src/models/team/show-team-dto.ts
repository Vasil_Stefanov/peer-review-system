import { IsEmail, IsString } from 'class-validator';
import { User } from '../../data/entities/user.entity';

export class ShowTeamDTO {
    @IsString()
    name: string;

    @IsString()
    users: User[];
}