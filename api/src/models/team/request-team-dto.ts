import { IsString, IsOptional } from 'class-validator';

export class RequestTeamDTO {
    @IsOptional()
    @IsString()
    name?: string;

    @IsOptional()
    @IsString()
    orderByDate?: 'ASC' | 'DESC';
}