import { IsString, IsArray } from 'class-validator';
import { User } from '../../data/entities/user.entity';

export class CreateTeamDTO {
    @IsString()
    name: string;

    @IsArray()
    users: string[];
}