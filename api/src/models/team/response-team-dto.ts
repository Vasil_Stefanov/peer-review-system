import { User } from '../../data/entities/user.entity';
import { Optional } from '@nestjs/common';
import { Expose } from 'class-transformer';
import { Invites } from '../../data/entities/invites.entity';
import { ShowReviewRequestDTO } from '../review-request/show-review-request-dto';

export class ResponseTeamDTO {
    @Expose()
    id: string;

    @Expose()
    @Optional()
    name?: string;

    @Optional()
    users?: Promise<User[]>;

    @Optional()
    invites?: Promise<Invites[]>;

    @Optional()
    reviewRequest?: ShowReviewRequestDTO[]

    @Optional()
    author?: User
}
