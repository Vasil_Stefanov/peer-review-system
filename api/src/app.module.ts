import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from './config/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigService } from './config/config.service';
import { TeamController } from './team/team.controller';
import { TeamModule } from './team/team.module';
import { ReviewRequestController } from './review-request/review-request.controller';
import { ReviewRequestModule } from './review-request/review-request.module';
import { NotificationController } from './notification/notification.controller';
import { NotificationModule } from './notification/notification.module';
import { ActivityModule } from './activity/activity.module';
import { CommentsController } from './comments/comments.controller';
import { CommentstModule } from './comments/comments.module';
import { AdminController } from './admin/admin.controller';
import { AdminModule } from './admin/admin.module';

// type, host, port, username, password, database, entities

@Module({
  imports: [
    CoreModule,
    AuthModule,
    ConfigModule,
    TeamModule,
    ReviewRequestModule,
    NotificationModule,
    ActivityModule,
    CommentstModule,
    AdminModule,
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => ({
        type: configService.dbType as any,
        host: configService.dbHost,
        port: configService.dbPort,
        username: configService.dbUsername,
        password: configService.dbPassword,
        database: configService.dbName,
        entities: ['./src/data/entities/*.ts'],
      }),
    }),
  ],
  controllers: [AppController, TeamController, ReviewRequestController, NotificationController, CommentsController, AdminController],
  providers: [],
})
export class AppModule {}
