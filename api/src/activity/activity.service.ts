import { Injectable, Post, BadRequestException } from '@nestjs/common';
import { User } from '../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../data/entities/notification.entity';
import { Activity } from '../data/entities/activity.entity';

@Injectable()
export class ActivityService {
    constructor(@InjectRepository(Activity) private readonly activityRepo: Repository<Activity>) {}
    async getActivity() {
        return await this.activityRepo.find({})
    }

    async newActivity(act: string) {
        const newActivity = new Activity();
        newActivity.activity = act;

        return await this.activityRepo.save(newActivity);
    }
}