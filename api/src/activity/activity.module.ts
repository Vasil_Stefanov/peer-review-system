import { User } from './../data/entities/user.entity';
import { ActivityService } from './activity.service';
import { Module } from '@nestjs/common';
import { Activity } from '../data/entities/activity.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { TeamSubscriber } from '../common/subscribers/team.subscriber';
import { Team } from '../data/entities/team.entity';
import { ReviewRequest } from '../data/entities/reviewRequest.entity';
import { ReviewRequestSubscriber } from '../common/subscribers/review-request.subscriber';

@Module({
  imports: [TypeOrmModule.forFeature([Activity, Team, ReviewRequest])],
  providers: [ActivityService, TeamSubscriber, ReviewRequestSubscriber],
  exports: [ActivityService],
})
export class ActivityModule {}
