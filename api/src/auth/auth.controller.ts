import { Controller, Post, Body, ValidationPipe, BadRequestException, Get, Param } from '@nestjs/common';
import { AuthService } from './auth.service';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { UserRegisterDTO } from '../models/user/user-register-dto';
import { User } from '../data/entities/user.entity';
import { SessionUser } from '../decorators/user.decorator';

@Controller('')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly usersService: UsersService,
  ) {}

  @Post('login')
  async login(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserLoginDTO): Promise<{token: string}> {
    
    const token = await this.authService.signIn(user);

    if (!token) {
      throw new BadRequestException(`Wrong credentials!`);
    }

    return token ;
  }

  @Post('register')
  async register(@Body(new ValidationPipe({
    transform: true,
    whitelist: true,
  })) user: UserRegisterDTO): Promise<User> {
    return await this.usersService.register(user);
  }

  @Get('users')
  async getAllUsers() {
    return await this.usersService.allUsers();
  }
  @Get('users/:username')
  async findUser(@Param('username') username: string){
    
    return await this.authService.validateUserExists(username);
  }
}
