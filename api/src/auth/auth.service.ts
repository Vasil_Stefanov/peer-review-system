import { Injectable, BadRequestException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UsersService } from '../core/services/users.service';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { JwtPayload } from '../core/interfaces/jwt-payload';
import { User } from '../data/entities/user.entity';

@Injectable()
export class AuthService {
  constructor(
    private readonly jwtService: JwtService,
    private readonly usersService: UsersService,
  ) { }

  async signIn(userData: UserLoginDTO): Promise<{ user: User; token: string }> {
    const user: User = await this.usersService.signIn(userData.name);
    
    if (!user) {
      throw new BadRequestException(`No such user`);
    }
    const isPasswordValid = await this.usersService.validatePassword(userData);
    if (!isPasswordValid) {
      throw new BadRequestException(`Passdowrd doesn't match`);
    }
    const userPayload: JwtPayload = { name: user.name };
    
    const token = await this.jwtService.sign(userPayload);
    return { user, token };
  }

  public async validateUserExists(username: string): Promise<any> {
    return await this.usersService.findUserByName(username);
  }
}
