import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { ReviewRequestController } from './review-request.controller';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [ReviewRequestController],
})
export class ReviewRequestModule {}