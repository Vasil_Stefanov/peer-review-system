import { Controller, Post, ValidationPipe, Body, UseGuards, Get, Param } from '@nestjs/common';
import { ReviewRequestService } from '../core/services/review-request.service';
import { SessionUser } from '../decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { CreateReviewRequestDTO } from '../models/review-request/create-review-request-dto';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard())

@Controller('review-request')
export class ReviewRequestController {
    constructor(private readonly reviewRequestService: ReviewRequestService)
    {}
    
    @Get()
    async getAllUserReviewRequests(@SessionUser() user: User) {
        return await this.reviewRequestService.getAllUserRR(user.name);
    }

    @Get('team')
    async getTeamReviewRequests(@SessionUser() user: User) {
        return await this.reviewRequestService.getTeamRR(user.name);
    }

    @Get(':title')
    async getReviewRequestByTitle(@Param('title') title: string) {
        return await this.reviewRequestService.findRR(title);
    }

    @Post()
    async createReviewRequest(
        @SessionUser() user: User, 
        @Body(new ValidationPipe({whitelist: true, transform: true})) reviewRequest: CreateReviewRequestDTO): Promise<CreateReviewRequestDTO> {
            return await this.reviewRequestService.createRR(user.name, reviewRequest)
    }

    @Post('vote')
    async voteOnReviewRequest(
        @SessionUser() user,
        @Body(new ValidationPipe({whitelist: true, transform: true})) vote) {
            console.log(vote);
            
        return await this.reviewRequestService.vote(vote, user);
    }

    @Post(':id')
    async closeReviewRequest(
        @Param('id') reviewReuqestId: string
    ) {
        return await this.reviewRequestService.closeRR(reviewReuqestId);
    }
}
