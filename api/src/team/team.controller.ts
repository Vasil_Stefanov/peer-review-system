import { Controller, Post, Req, Body, ValidationPipe, Get, Query, UseGuards, Delete, Param } from '@nestjs/common';
import { CreateTeamDTO } from '../models/team/create-team-dto';
import { ShowTeamDTO } from '../models/team/show-team-dto';
import { TeamService } from '../core/services/team.service';
import { ResponseTeamDTO } from '../models/team/response-team-dto';
import { RequestTeamDTO } from '../models/team/request-team-dto';
import { SessionUser } from '../decorators/user.decorator';
import { User } from '../data/entities/user.entity';
import { AuthGuard } from '@nestjs/passport';

@UseGuards(AuthGuard())

@Controller('team')
export class TeamController {
    constructor(private readonly teamService: TeamService) {}

    @Get()
    async getAllTeams(@SessionUser() user: User): Promise<ResponseTeamDTO> {
        return await this.teamService.showAllTeams(user);
    }
    @Get(':name')
    async getTeamUsersByName(@Param('name') teamName: string) {
        return await this.teamService.getTeamByName(teamName);
    }
    @Post()
    async create(@SessionUser() user: User, @Body(new ValidationPipe({whitelist: true, transform: true})) team: CreateTeamDTO): Promise<ResponseTeamDTO> {
        
        return await this.teamService.createTeam(user.name,team.name, team.users);
    }
    @Post('respond')
    async resolveInvite(@SessionUser() user: User, @Body(new ValidationPipe({whitelist: true, transform: true})) response) {
        return await this.teamService.resolveInvite(user.name, response);
    }
    @Delete(':id')
    async leaveTeam(@SessionUser() user: User, @Param('id') teamId: string) {
        return await this.teamService.leaveTeam(user.name, teamId);
    }
}
