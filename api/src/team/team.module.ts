import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { TeamController } from './team.controller';
import { Module } from '@nestjs/common';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [TeamController],
})
export class TeamModule {}