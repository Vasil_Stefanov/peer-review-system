import { Module } from '@nestjs/common';
import { UsersService } from './services/users.service';
import { TypeOrmModule } from '@nestjs/typeorm';
import { User } from '../data/entities/user.entity';
import { TeamService } from './services/team.service';
import { Team } from '../data/entities/team.entity';
import { ReviewRequestService } from './services/review-request.service';
import { ReviewRequest } from '../data/entities/reviewRequest.entity';
import { ReviewRequestState } from '../data/entities/reviewRequestState.entity';
import { Notification } from '../data/entities/notification.entity';
import { NotificationService } from './services/notification.service';
import { Invites } from '../data/entities/invites.entity';
import { ActivityService } from '../activity/activity.service';
import { Activity } from '../data/entities/activity.entity';
import { CommentsService } from './services/comments.service';
import { Comment } from '../data/entities/comment.entity';
import { AdminService } from './services/admin.service';

@Module({
  imports: [TypeOrmModule.forFeature([User, Team, ReviewRequest, ReviewRequestState, Notification, Invites, Activity, Comment])],
  providers: [UsersService, TeamService, ReviewRequestService, NotificationService, ActivityService, CommentsService, AdminService],
  exports: [UsersService, TeamService, ReviewRequestService, NotificationService, ActivityService, CommentsService, AdminService],
})
export class CoreModule {}
