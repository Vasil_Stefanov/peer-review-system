import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ReviewRequest } from '../../data/entities/reviewRequest.entity';
import { Repository } from 'typeorm';
import { User } from '../../data/entities/user.entity';
import { Team } from '../../data/entities/team.entity';
import { ReviewRequestState } from '../../data/entities/reviewRequestState.entity';
import { UserRole } from '../../common/enums/user-role.enum';
import { Status } from '../../common/enums/status.enum';
import { Activity } from '../../data/entities/activity.entity';
import { _ } from 'lodash';

@Injectable()
export class AdminService {
    constructor(
        @InjectRepository(ReviewRequest) private readonly reviewRequestRepo: Repository<ReviewRequest>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Team) private readonly teamRepo: Repository<Team>,
        @InjectRepository(ReviewRequestState) private readonly stateRepo: Repository<ReviewRequestState>,
        @InjectRepository(Activity) private readonly activityRepo: Repository<Activity>,
    ) { }

    async getAllUsers() {
        const allUsers = await this.userRepo.find({});
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: 'h3h3'
            }
        })
        const members = await foundTeam.users;

        return await this.userRepo.find({});
    }

    async getNonTeamUsers(name) {
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: name
            }
        })
        const users = await this.userRepo.find({});
    }

    async getAllUserRequests() {
        return await this.reviewRequestRepo.find({})
    }

    async getAllTeamRequests() {
        const teams = await this.teamRepo.find({});
        const requests = [];
        const teamReviewRequests = teams.map(async x => requests.push(await x.reviewRequest))
        await Promise.all(teamReviewRequests);
        return requests
    }

    async getAllTeams() {
        return await this.teamRepo.find({});
    }

    async assignAsAdmin(name) {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: name
            }
        });
        foundUser.roles = UserRole.admin;
        return await this.userRepo.save(foundUser)
    }

    async manipulateRequest(request) {
        const foundRequest = await this.reviewRequestRepo.findOne({
            where: {
                id: request.id
            }
        })
        
        switch (request.act) {
            case 'accepted':
                foundRequest.status = Status.Accepted
                foundRequest.isClosed = true;
                await this.reviewRequestRepo.save(foundRequest);
                break;
            case 'rejected':
                foundRequest.status = Status.Rejected
                foundRequest.isClosed = true;
                await this.reviewRequestRepo.save(foundRequest);
                break;
            default:
                break;
        }
    }

    async moveUser(act) {
        //act.userName, act.teamName, act.action, act.target
        const foundUser = await this.userRepo.findOne({
            where: {
                name: act.userName
            }
        });
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: act.teamName
            }
        });

        switch (act.action) {
            case 'remove':

                foundUser.teams = Promise.resolve((await foundUser.teams).filter(x => x.name !== foundTeam.name));
                await this.userRepo.save(foundUser);
                break;
            case 'add':
                foundUser.teams = Promise.resolve([foundTeam, ...await foundUser.teams]);
                await this.userRepo.save(foundUser);
                break;
            default:
                break;
        }
    }

    async getActivity() {
        return await this.activityRepo.find({});
    }
}
