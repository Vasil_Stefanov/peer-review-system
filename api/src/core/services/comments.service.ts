import { Injectable, Post, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../../data/entities/notification.entity';
import { Comment } from '../../data/entities/comment.entity';
import { ReviewRequest } from '../../data/entities/reviewRequest.entity';

@Injectable()
export class CommentsService {
    constructor(
        @InjectRepository(Comment) private readonly commentsRepo: Repository<Comment>,
        @InjectRepository(ReviewRequest) private readonly requestRepo: Repository<ReviewRequest>,
        @InjectRepository(User) private readonly userRepo: Repository<User>

    ) { }

    async getAllComments(requestTitle) {
        const foundRequest = await this.requestRepo.findOne({
            where: {
                title: requestTitle
            }
        });
        return await foundRequest.comments;
    }

    async createComment(comment, userId) {
        const foundRequest = await this.requestRepo.findOne({
            where: {
                title: comment.title
            }
        });

        const foundUser = await this.userRepo.findOne({
            where: {
                id: userId
            }
        });
        const commentToAdd = new Comment();
        commentToAdd.author = await foundUser.name;
        commentToAdd.reviewRequest = Promise.resolve(foundRequest);
        commentToAdd.content = comment.content;
        await this.commentsRepo.save(commentToAdd);
        // foundRequest.comments = Promise.resolve([commentToAdd, ...await foundRequest.comments]);
        // await this.requestRepo.save(foundRequest)
        await this.userRepo.save(foundUser);
        const returnObj = {
            author: foundUser.name,
            content: commentToAdd.content
        }
        return await returnObj;
    }
}