import { Injectable, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user.entity';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as bcrypt from 'bcrypt';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
  ) { }

  async signIn(username: string): Promise<User> {
    const user = await this.userRepo.findOne({
      where: {
        name: username,
      },
    });
    if (!user) {
      throw new BadRequestException(`User with username ${username} does not exist!`);
    }

    return user;
  }
  public async findUserByName(username: string): Promise<any> {
    return await this.userRepo.findOne({ where: { name: username } });

  }
  async register(userData: UserRegisterDTO): Promise<User | undefined> {
    const userByUsername = await this.userRepo.findOne({
      where: { name: userData.name },
    });
    if (userByUsername) {
      throw new BadRequestException('This username is already taken!');
    }

    const passwordHash = await bcrypt.hash(userData.password, 10);

    const savedUser = await this.userRepo.save({
      ...userData,
      password: passwordHash,
    });

    return savedUser;
  }

  async validate(payload: JwtPayload): Promise<User | undefined> {
    return await this.userRepo.findOne({
      where: {
        ...payload,
      },
    });
  }

  async validatePassword(user: UserLoginDTO): Promise<boolean> {
    const userEntity = await this.userRepo.findOne({
      where: {
        name: user.name,
      },
    });

    return await bcrypt.compare(user.password, userEntity.password);
  }

  async allUsers() {
    return await this.userRepo.find({});
  }
}
