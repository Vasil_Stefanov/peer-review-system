import { Injectable, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Team } from '../../data/entities/team.entity';
import { plainToClass } from 'class-transformer';
import { ResponseTeamDTO } from '../../models/team/response-team-dto';
import { Notification } from '../../data/entities/notification.entity';
import { Invites } from '../../data/entities/invites.entity';
import { TeamStatus } from '../../common/enums/team-status.enum';
import { ActivityService } from '../../activity/activity.service';

@Injectable()
export class TeamService {
    constructor(
        @InjectRepository(Team) private readonly teamRepo: Repository<Team>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Notification) private readonly notificationRepo: Repository<Notification>,
        @InjectRepository(Invites) private readonly invitesRepo: Repository<Invites>,
        private readonly activityService: ActivityService
    ) { }

    async createTeam(currentUser: string, name: string, users: string[]): Promise<ResponseTeamDTO> {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: currentUser
            }
        })

        const teamName = await this.teamRepo.findOne({
            where: { name: name }
        });

        if (teamName) {
            throw new BadRequestException('Team with this name already exists')
        }
        const teamToCreate: Team = new Team();
        teamToCreate.name = name;
        teamToCreate.author = Promise.resolve(foundUser)
        await this.teamRepo.save(teamToCreate);
        foundUser.teams = Promise.resolve([teamToCreate, ...await foundUser.teams]);
        await this.userRepo.save(foundUser);
        const members = users.map(async x => {
            const foundMember = await this.userRepo.findOne({
                where: {
                    name: x
                }
            });
            const invite = new Invites();
            invite.team = teamToCreate.name
            invite.user = Promise.resolve(foundMember)
            await this.invitesRepo.save(invite);
            foundMember.invites = Promise.resolve([invite, ...await foundMember.invites])
            await this.userRepo.save(foundMember);
        });
        await Promise.all(members)
        this.sendNotification(users, `You have been added to team ${teamToCreate.name}`, `team:${teamToCreate.name}`);

        return plainToClass(ResponseTeamDTO, teamToCreate, { excludeExtraneousValues: true });
    }
    //where: {
    //  name: In(users)
    //}
    async showAllTeams(user): Promise<any> {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: user.name
            }
        })
        const allTeams = await foundUser.teams;
        const invites = await foundUser.invites;
        const allPendingInvites = invites.filter(x => x.status === TeamStatus.Pending)
        const returnObj = {
            teams: allTeams,
            invites: allPendingInvites
        }
        return await returnObj;
    }

    async leaveTeam(user, teamId): Promise<any> {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: user
            }
        });
        const foundTeam = await this.teamRepo.findOne({
            where: {
                id: teamId
            }
        })
        const users = await foundTeam.users;

        const teams = await foundUser.teams;
        foundUser.teams = Promise.resolve(await teams.filter(x => x.id !== teamId));
        await this.userRepo.save(foundUser);

        const act = `${foundUser.name} left team ${foundTeam.name}`;
        await this.activityService.newActivity(act);
        this.sendNotification(users, `${foundUser.name} has left team ${foundTeam.name}`, `team:${foundTeam.name}`);
        return await foundUser.teams;
    }

    async sendNotification(users, message, key) {

        const notification = new Notification();

        const members = users.map(async x => {

            const foundMember = await this.userRepo.findOne({
                where: {
                    name: x.name
                }
            });

            notification.content = message;
            notification.key = key;
            notification.user = Promise.resolve(foundMember);
            await this.notificationRepo.save(await notification);
        });
        Promise.all(await members);
    }

    async resolveInvite(userName, response) {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: userName
            }
        });
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: response.teamName
            }
        });
        const foundInvite = await this.invitesRepo.findOne({
            where: {
                id: response.id
            }
        });
        // const members = users.map(async x => {
        //     const foundMember = await this.userRepo.findOne({
        //         where: {
        //             name: x.name
        //         }
        //     });
        //     foundMember.teams = Promise.resolve([teamToCreate, ...await foundMember.teams]);
        //     await this.userRepo.save(foundMember);
        // });
        // await Promise.all(members)
        let act = '';

        switch (response.status) {
            case 'accept':
                foundInvite.status = TeamStatus.Accepted;
                foundUser.teams = Promise.resolve([foundTeam, ...await foundUser.teams]);
                act = `${foundUser.name} accepted invite to join team ${foundTeam.name}`;
                await this.activityService.newActivity(act)
                await this.invitesRepo.save(foundInvite);
                await this.userRepo.save(foundUser);
                break;

            default:
                foundInvite.status = TeamStatus.Rejected;
                act = `${foundUser.name} rejected invite to join team ${foundTeam.name}`;
                
                await this.activityService.newActivity(act)
                await this.invitesRepo.save(foundInvite);
                break;
        }
    }

    async getTeamByName(name: string) {
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: name
            }
        });
        const teamMembers = await foundTeam.users;
        return await teamMembers;
    }
}