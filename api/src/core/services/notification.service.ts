import { Injectable, Post, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Notification } from '../../data/entities/notification.entity';

@Injectable()
export class NotificationService {
    constructor(
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Notification) private readonly notificationRepo: Repository<Notification>
    ) { }
    public async getNotifications(userName: string): Promise<any[]> {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: userName
            }
        })
        const foundNotifications = await foundUser.notification;
        const returnNotifications = foundNotifications.filter(x => x.checked === false);
        return await returnNotifications
    }

    public async updateNotification(id: string): Promise<any> {
        const foundNotification = await this.notificationRepo.findOne({
            where: {
                id: id
            }
        })
        
        foundNotification.checked = true;
        return await this.notificationRepo.save(foundNotification);
    }
}