import { Injectable, Post, BadRequestException } from '@nestjs/common';
import { User } from '../../data/entities/user.entity';
import { UserLoginDTO } from '../../models/user/user-login-dto';
import { JwtPayload } from '../interfaces/jwt-payload';
import { UserRegisterDTO } from '../../models/user/user-register-dto';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Team } from '../../data/entities/team.entity';
import { plainToClass } from 'class-transformer';
import { ReviewRequest } from '../../data/entities/reviewRequest.entity';
import { ShowReviewRequestDTO } from '../../models/review-request/show-review-request-dto';
import { CreateReviewRequestDTO } from '../../models/review-request/create-review-request-dto';
import { Status } from '../../common/enums/status.enum';
import { ReviewRequestState } from '../../data/entities/reviewRequestState.entity';
import { Notification } from '../../data/entities/notification.entity';

@Injectable()
export class ReviewRequestService {
    constructor(
        @InjectRepository(ReviewRequest) private readonly reviewRequestRepo: Repository<ReviewRequest>,
        @InjectRepository(User) private readonly userRepo: Repository<User>,
        @InjectRepository(Team) private readonly teamRepo: Repository<Team>,
        @InjectRepository(ReviewRequestState) private readonly stateRepo: Repository<ReviewRequestState>,
        @InjectRepository(Notification) private readonly notificationRepo: Repository<Notification>
    ) { }

    async createRR(userName: string, reviewRequest: CreateReviewRequestDTO): Promise<ShowReviewRequestDTO> {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: userName
            }
        });
        const foundTeam = await this.teamRepo.findOne({
            where: {
                name: reviewRequest.team
            }
        })
        const foundReviewRequest = await this.reviewRequestRepo.findOne({
            where: {
                title: reviewRequest.title
            }
        })

        if (await foundReviewRequest) {
            throw new BadRequestException('Post with this title already exists');
        }
        const reviewers = reviewRequest.reviewers;
        const newReviewRequest = new ReviewRequest();
        const newReviewState = new ReviewRequestState();
        newReviewRequest.assignee = Promise.resolve(foundUser);
        newReviewRequest.author = foundUser.name;
        newReviewRequest.title = reviewRequest.title;
        newReviewRequest.content = reviewRequest.content;
        newReviewRequest.tags = reviewRequest.tags.toString();
        newReviewRequest.status = Status.Pending;
        await this.reviewRequestRepo.save(newReviewRequest);
        newReviewState.reviewRequestId = await newReviewRequest.id;
        await this.stateRepo.save(newReviewState);
        foundTeam.reviewRequest = Promise.resolve([newReviewRequest, ...await foundTeam.reviewRequest])
        await this.teamRepo.save(foundTeam);
        const newReviewers = reviewers.map(async x => {
            const foundMember = await this.userRepo.findOne({
                where: {
                    name: x
                }
            });
            foundMember.reviews = Promise.resolve([newReviewRequest, ...await foundMember.reviews]);
            await this.userRepo.save(foundMember);
        });

        await Promise.all(newReviewers)
        this.sendNotification(newReviewRequest, `You have been added as a reviewer to '${newReviewRequest.title}' review request`, `request:${newReviewRequest.title}`);
        return await plainToClass(ShowReviewRequestDTO, newReviewRequest, { excludeExtraneousValues: true });
    }

    async findRR(requestTitle: string) {
        const foundRequest = await this.reviewRequestRepo.findOne({
            where: {
                title: requestTitle
            }
        });
        const returnObj = {
            request: await foundRequest,
            voters: await foundRequest.voters
        }
        return await returnObj;
    }

    async getAllUserRR(userName: string) {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: userName
            }
        });
        const reviewRequests = await foundUser.reviewRequest;


        return await reviewRequests;
    }

    async getTeamRR(userName: string) {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: userName
            }
        });
        const userTeams = await foundUser.teams;
        const teamReviewRequests = await userTeams.map(async x => {
            const foundTeam = await this.teamRepo.findOne({
                where: {
                    name: x.name
                }
            });
            const reviewRequests = await foundTeam.reviewRequest;

            return await reviewRequests;
        })
        return await Promise.all(teamReviewRequests);
    }

    async closeRR(id: string) {
        const foundReviewRequest = await this.reviewRequestRepo.findOne({
            where: {
                id: id
            }
        });
        this.sendNotification(foundReviewRequest, `Review request '${foundReviewRequest.title}' has been closed`, `request:${foundReviewRequest.title}`);
        foundReviewRequest.isClosed = true;
        return await this.reviewRequestRepo.save(foundReviewRequest);
    }

    async vote(vote, user) {
        const foundUser = await this.userRepo.findOne({
            where: {
                name: user.name
            }
        });
        const foundReviewRequest = await this.reviewRequestRepo.findOne({
            where: {
                id: vote.id,
                isClosed: false
            }
        });
        const foundReviewRequestState = await this.stateRepo.findOne({
            where: {
                reviewRequestId: await foundReviewRequest.id
            }
        })
        
        switch (vote.status) {
            case 'accepted':
                if (await foundReviewRequestState.Accepted < 3) {
                    foundReviewRequestState.Accepted += 1;
                    foundReviewRequest.voters = Promise.resolve([foundUser, ...await foundReviewRequest.voters]);
                    await this.reviewRequestRepo.save(foundReviewRequest);
                    await this.stateRepo.save(foundReviewRequestState);
                    if (await foundReviewRequestState.Accepted === 3) {
                        foundReviewRequest.status = Status.Accepted;
                        foundReviewRequest.isClosed = true;
                        await this.reviewRequestRepo.save(foundReviewRequest);
                        this.sendNotification(foundReviewRequest, `Review request '${foundReviewRequest.title}'' has been closed with status 'Accepted'`, `request:${foundReviewRequest.title}`);
                    }
                }
                break;
            case 'rejected':
                foundReviewRequestState.Rejected += 1;
                await this.stateRepo.save(foundReviewRequestState);
                foundReviewRequest.status = Status.Rejected;
                foundReviewRequest.isClosed = true;
                foundReviewRequest.voters = Promise.resolve([foundUser, ...await foundReviewRequest.voters]);
                await this.reviewRequestRepo.save(foundReviewRequest);
                this.sendNotification(foundReviewRequest, `Review request '${foundReviewRequest.title}'' has been closed with status 'Rejected'`, `request:${foundReviewRequest.title}`);
                break;
            case 'changerequested':
                if (await foundReviewRequestState.ChangeRequested < 3) {
                    foundReviewRequestState.ChangeRequested += 1;
                    foundReviewRequest.voters = Promise.resolve([foundUser, ...await foundReviewRequest.voters]);
                    await this.reviewRequestRepo.save(foundReviewRequest);
                    await this.stateRepo.save(foundReviewRequestState);
                    if (await foundReviewRequestState.ChangeRequested === 3) {
                        foundReviewRequest.status = Status.ChangeRequested;
                        await this.reviewRequestRepo.save(foundReviewRequest);
                        this.sendNotification(foundReviewRequest, `Review request '${foundReviewRequest.title}'' status has been set to 'Change Requested'`, `request:${foundReviewRequest.title}`);
                    }
                }
                break;
            default:
                foundReviewRequest.status = Status.UnderReview;
                await this.reviewRequestRepo.save(foundReviewRequest);
                this.sendNotification(foundReviewRequest, `Review request '${foundReviewRequest.title}'' status has been set to 'Under Review'`, `request:${foundReviewRequest.title}`);
                break;
        }
    }

    async sendNotification(foundReviewRequest, message, key) {

        const notification = new Notification();
        const reviewers = await foundReviewRequest.users;

        const users = reviewers.map(async x => {

            const foundMember = await this.userRepo.findOne({
                where: {
                    name: x.name
                }
            });

            notification.content = message;
            notification.key = key;
            notification.user = Promise.resolve(foundMember);
            await this.notificationRepo.save(await notification);
        });
        Promise.all(await users);
    }
}