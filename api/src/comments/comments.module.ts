import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { CommentsController } from './comments.controller';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [CommentsController],
})
export class CommentstModule {}