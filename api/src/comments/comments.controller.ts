import { Controller, Get, Param, Body, ValidationPipe, UseGuards, Post } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SessionUser } from '../decorators/user.decorator';
import { CommentsService } from '../core/services/comments.service';
@UseGuards(AuthGuard())

@Controller('comments')
export class CommentsController {
    constructor(
        private readonly commentsService: CommentsService
    ) {}

    @Get(':title')
    async getAllComments(@Param('title') requestTitle: string) {
        return await this.commentsService.getAllComments(requestTitle);
    }

    @Post('')
    async createComment(@Body() comment, @SessionUser() user) {
        return await this.commentsService.createComment(comment, user.id);
    }
}
