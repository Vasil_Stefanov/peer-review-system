import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { NotificationController } from './notification.controller';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [NotificationController],
})
export class NotificationModule {}