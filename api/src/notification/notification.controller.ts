import { Controller, Get, UseGuards, Post, Param } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { SessionUser } from '../decorators/user.decorator';
import { UserLoginDTO } from '../models/user/user-login-dto';
import { NotificationService } from '../core/services/notification.service';
@UseGuards(AuthGuard())

@Controller('notification')
export class NotificationController {
    constructor(private readonly notificationService: NotificationService)
    {}

    @Get()
    public async getNotifications(@SessionUser() user: UserLoginDTO): Promise<any[]> {
        return await this.notificationService.getNotifications(user.name);
    }

    @Post(':id')
    public async updateNotification(@Param('id') id: string): Promise<any> {
        return await this.notificationService.updateNotification(id);
    }
}
