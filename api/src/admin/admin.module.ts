import { CoreModule } from '../core/core.module';
import { AuthModule } from '../auth/auth.module';
import { Module } from '@nestjs/common';
import { AdminController } from './admin.controller';

@Module({
    imports: [CoreModule, AuthModule],
    controllers: [AdminController],
})
export class AdminModule {}