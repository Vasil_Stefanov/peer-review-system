import { Controller, UseGuards, Get, Post, Body, Param } from '@nestjs/common';
import { Roles } from '../common/decorators/roles.decorator';
import { AuthGuard } from '@nestjs/passport';
import { RolesGuard } from '../common/guards/roles.guard';
import { AdminService } from '../core/services/admin.service';

@Roles('admin')
@UseGuards(AuthGuard(), RolesGuard)
@Controller('control')
export class AdminController {
    constructor(
        private readonly adminService: AdminService
    ) { }
    @Get('users')
    async getAllUsers() {
        return await this.adminService.getAllUsers();
    }
    @Get('user-requests')
    async getAllUserRequests() {
        return await this.adminService.getAllUserRequests();
    }

    @Get('team-requests')
    async getAllTeamRequests() {
        return await this.adminService.getAllTeamRequests();
    }

    @Get('teams')
    async getAllTeams() {
        return await this.adminService.getAllTeams();
    }

    @Post('assign-admin/:name')
    async assignAsAdmin(@Param('name') userName:string) {
        return await this.adminService.assignAsAdmin(userName);
    }

    @Post('manipulate-request')
    async manipulateRequest(@Body() act) {
        return await this.adminService.manipulateRequest(act);
    }

    @Post('move-user')
    async moveUser(@Body() act) {
        return await this.adminService.moveUser(act);
    }

    @Get('activity')
    async getActivity() {
        return await this.adminService.getActivity();
    }
}
