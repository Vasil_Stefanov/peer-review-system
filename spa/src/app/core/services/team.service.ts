
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class TeamService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getTeams(): any {
    return this.http.get('http://localhost:3000/team');
  }

  public createTeam(team): any {
    return this.http.post('http://localhost:3000/team', team);
  }

  public teamMembers(name): any {
    return this.http.get(`http://localhost:3000/team/${name}`);
  }

  public resolveInvite(response): any {
    return this.http.post('http://localhost:3000/team/respond', response);
  }

  public leaveTeam(id) {
    return this.http.delete(`http://localhost:3000/team/${id}`);
  }
}
