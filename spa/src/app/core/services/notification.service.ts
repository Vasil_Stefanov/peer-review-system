import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class NotificationService {

  constructor(
    private readonly http: HttpClient,
  ) { }

  public getNotifications(): Observable<any[]> {
      return this.http.get<any[]>('http://localhost:3000/notification');
  }

  public updateNotification(id): Observable<any[]> {
      return this.http.post<any[]>(`http://localhost:3000/notification/${id}`, id);
  }
}
