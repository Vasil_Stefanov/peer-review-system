// import { UserRegister } from './../../common/models/user-register';
import { StorageService } from './storage.service';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

// import { UserLogin } from '../../common/models/user-login';
// import { User } from '../../common/models/user';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Router } from '@angular/router';
import { User } from 'src/app/common/models/user';
import { UserLogin } from 'src/app/common/models/user-login';
import { UserRegister } from 'src/app/common/models/user-register';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private isAuthenticatedSubject$: BehaviorSubject<string | null> = new BehaviorSubject(this.username);
  private currentUserSubject$: BehaviorSubject<User> = new BehaviorSubject({} as User);

  constructor(
    private readonly http: HttpClient,
    private readonly storageService: StorageService,
    private readonly jwtHelper: JwtHelperService,
    private readonly router: Router,
  ) {
    if (!this.username ) {
      this.router.navigate(['/signIn']);
    } else if (this.isTokenExpired) {
      this.isAuthenticatedSubject$.next(null);
      this.router.navigate(['/signIn']);
    } else {
      this.http.get(`http://localhost:3000/users/${this.username}`).subscribe(
        (res: any) => {
          this.isAuthenticatedSubject$.next(res.name);
          this.currentUserSubject$.next(res);
        }
      );
    }
  }

  public get isAuthenticated$(): Observable<string> {
    return this.isAuthenticatedSubject$.asObservable();
  }

  public get currentUser$(): Observable<User> {
    return this.currentUserSubject$.asObservable();
  }

  private get username(): string | null {
    const token = this.storageService.get('token');
    if (token) {
      return this.jwtHelper.decodeToken(token).username;
    }

    return null;
  }

  private get isTokenExpired(): boolean {
    const token = this.storageService.get('token');
    if (token) {
      return this.jwtHelper.isTokenExpired();
    }

    return false;
  }

  public login(user: UserLogin): Observable<any> {
    return this.http.post('http://localhost:3000/login', user).pipe(
      tap((res: any) => {
        this.isAuthenticatedSubject$.next(res.user.name);
        this.currentUserSubject$.next(res.user);
        this.storageService.set('token', res.token);
      })
    );
  }

  public register(newUser: UserRegister) {
    return this.http.post('http://localhost:3000/register', newUser);
  }

  public logout() {
    const token = this.storageService.get('token');
        this.isAuthenticatedSubject$.next(null);
        this.currentUserSubject$.next({} as User);
        this.storageService.remove('token');
  }
}
