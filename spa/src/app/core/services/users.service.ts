
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class UsersService {

    constructor(
        private readonly http: HttpClient,
    ) {}

  public allUsers(): any {
    return this.http.get('http://localhost:3000/users');
  }
}
