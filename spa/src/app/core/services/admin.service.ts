import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable()
export class AdminService {

    constructor(
        private readonly http: HttpClient,
    ) { }

    public getAllUsers() {
        return this.http.get<any[]>('http://localhost:3000/control/users');
    }

    public getAllUserRequests() {
        return this.http.get<any[]>('http://localhost:3000/control/user-requests');
    }

    public getAllTeamRequests() {
        return this.http.get<any[]>('http://localhost:3000/control/team-requests');
    }

    public getAllTeams() {
        return this.http.get<any[]>('http://localhost:3000/control/teams');
    }

    public assignAsAdmin(user) {
        return this.http.post(`http://localhost:3000/control/assign-admin/${user}`, user);
    }

    public manipulateRequest(act) {
        return this.http.post<any[]>(`http://localhost:3000/control/manipulate-request`, act);
    }

    public moveUser(act) {
        return this.http.post<any[]>('http://localhost:3000/control/move-user', act);
    }

    public getActivity() {
        return this.http.get<any[]>('http://localhost:3000/control/activity');
    }
}
