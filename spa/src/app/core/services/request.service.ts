import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class RequestService {

    constructor(
        private readonly http: HttpClient,
    ) {}

    public allLoggedUserRequests(): Observable<any[]> {
        return this.http.get<any[]>('http://localhost:3000/review-request');
    }
    public getTeamRequests(): Observable<any[]> {
        return this.http.get<any[]>('http://localhost:3000/review-request/team');
    }
    public createReviewRequest(request): Observable<any> {
        return this.http.post<any>('http://localhost:3000/review-request', request);
    }

    public getReviewRequestByTitle(title): Observable<any> {
        return this.http.get<any>(`http://localhost:3000/review-request/${title}`);
    }

    public updateStatus(vote): Observable<any> {
        return this.http.post('http://localhost:3000/review-request/vote', vote);
    }
}
