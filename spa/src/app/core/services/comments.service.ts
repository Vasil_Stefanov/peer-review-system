import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class CommentsService {
    constructor(
        private readonly http: HttpClient,
    ) {}

    public getPostComments(title): Observable<any[]> {
        return this.http.get<any>(`http://localhost:3000/comments/${title}`);
    }

    public createComments(comment) {
        return this.http.post('http://localhost:3000/comments', comment);
    }
}
