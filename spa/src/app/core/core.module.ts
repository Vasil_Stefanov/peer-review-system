import { NotificatorService } from './services/notificator.service';
import { NgModule, Optional, SkipSelf } from '@angular/core';
import { AuthService } from './services/auth.service';
import { StorageService } from './services/storage.service';
import { HttpClientModule } from '@angular/common/http';
import { ToastrModule } from 'ngx-toastr';
import { RequestService } from './services/request.service';
import { UsersService } from './services/users.service';
import { TeamService } from './services/team.service';
import { SharedService } from './services/shared.service';
import { NotificationService } from './services/notification.service';
import { CommentsService } from './services/comments.service';
import { AdminService } from './services/admin.service';

@NgModule({
  imports: [
    ToastrModule.forRoot({
      positionClass: 'toast-bottom-center',
      preventDuplicates: true,
    }),
    HttpClientModule
  ],
  providers: [
    AuthService,
    StorageService,
    NotificatorService,
    RequestService,
    UsersService,
    TeamService,
    SharedService,
    NotificationService,
    CommentsService,
    AdminService,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parent: CoreModule) {
    if (parent) {
      throw new Error('Core module is already provided!');
    }
  }
}
