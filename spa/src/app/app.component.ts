import { AuthService } from './core/services/auth.service';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { NotificatorService } from './core/services/notificator.service';
import { Router, ActivatedRoute, NavigationEnd } from '@angular/router';
import { filter } from 'rxjs/operators';
import { SharedService } from './core/services/shared.service';
import { NotificationService } from './core/services/notification.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit, OnDestroy {
  public username: string;
  public isLogged: boolean;
  private subscription: Subscription;
  public isHome: boolean;
  public notifications = [];
  public hasNotifications = false;
  constructor(
    private readonly authService: AuthService,
    private readonly notificator: NotificatorService,
    private readonly router: Router,
    private readonly activatedRoute: ActivatedRoute,
    private readonly sharedService: SharedService,
    private readonly notificationService: NotificationService
  ) {
    sharedService.changeEmitted$.subscribe(
      text => {
          this.notifications = text;
          if (this.notifications.length > 0) {
            this.hasNotifications = true;
          } else {
            this. hasNotifications = false;
          }
      });
  }

  ngOnInit(): void {
    this.username = '';
    this.isLogged = false;
    this.subscription = this.authService.isAuthenticated$.subscribe(username => {
      if (username === null) {
        this.username = '';
        this.isLogged = false;
      } else {
        this.username = username;
        this.isLogged = true;
      }
    });

    this.router.events.pipe(
      filter((event: any) => event instanceof NavigationEnd)
    ).subscribe(event => {
      this.isHome = event.url === '' ? true : false;
    });
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }

  logout() {
    this.authService.logout();
    this.notificator.success('Logout successfull!');
    this.router.navigate(['/signIn']);
  }

  updateNotification(notification) {
    this.notificationService.updateNotification(notification.id).subscribe();
    const key = notification.key.split(':');

    this.notifications = this.notifications.filter(x => x.id !== notification.id);
    if (this.notifications.length === 0) {
      this.hasNotifications = false;
    }
    if (key[0] === 'team') {
      this.router.navigate(['/profile', this.username]);
    } else {
      this.router.navigate(['/requests', key[1]]);
    }
  }
}
