import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { DatePipe } from './date.pipe';
import { SharedModule } from 'src/app/shared/shared.module';
import { FormsModule } from '@angular/forms';


@NgModule({
  declarations: [DatePipe],
  imports: [SharedModule, FormsModule, CommonModule],
  exports: [DatePipe]
})

export class MainPipe {}
