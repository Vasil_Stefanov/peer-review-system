export interface User {
    id: string;
    name: string;
    password: string;
    roles: string;
    createdOn: Date;
    updatedOn: Date;
}
