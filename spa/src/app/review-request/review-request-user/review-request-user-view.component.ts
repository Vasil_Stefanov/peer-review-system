import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-review-request-user',
  templateUrl: './review-request-user-view.component.html',
  styleUrls: ['./review-request-user-view.component.css']
})
export class ReviewRequestUserViewComponent implements OnInit {
  @Input()
  public request;
  @Input()
  public loggedUser;
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
    switch (this.request.status) {
      case 0:
      this.request.status = 'Pending';
        break;
      case 1:
        this.request.status = 'Under Review';
        break;
      case 2:
        this.request.status = 'Change Requested';
        break;
      default:
        break;
    }
  }

}
