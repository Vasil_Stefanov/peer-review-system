import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-user-requests',
  templateUrl: './user-requests.component.html',
  styleUrls: ['./user-requests.component.css']
})
export class UserRequestsComponent implements OnInit {
  @Input()
  public request;
  @Input()
  public loggedUser;
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
    switch (this.request.status) {
      case 0:
        this.request.status = 'Pending';
        break;
      case 1:
        this.request.status = 'Under Review';
        break;
      case 2:
        this.request.status = 'Change Requested';
        break;
      case 3:
        this.request.status = 'Accepted';
        break;
      case 4:
        this.request.status = 'Rejected';
        break;
      default:
        break;
    }
  }
}
