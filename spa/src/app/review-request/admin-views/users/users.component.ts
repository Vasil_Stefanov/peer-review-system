import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  @Input()
  public user;
  @Input()
  public loggedUser;
  @Output()
  adminAssigned = new EventEmitter();
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
  }
  assignAsAdmin(name) {
    this.adminAssigned.emit(name);
  }
}
