import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { UsersService } from 'src/app/core/services/users.service';
import { TeamService } from 'src/app/core/services/team.service';
import { User } from 'src/app/common/models/user';
import _ from 'lodash';
import { AdminService } from 'src/app/core/services/admin.service';

@Component({
  selector: 'app-teams',
  templateUrl: './teams.component.html',
  styleUrls: ['./teams.component.css']
})
export class TeamsComponent implements OnInit {
  @Input()
  public team;
  @Input()
  public loggedUser;
  public teamMembers: string[];
  public users: string[];
  public difference: string[];
  constructor(
    private readonly userService: UsersService,
    private readonly teamService: TeamService,
    private readonly adminService: AdminService
  ) { }

  ngOnInit() {
    this.teamService.teamMembers(this.team.name).subscribe(data => {
      this.teamMembers = data.map(x => x.name);
      this.userService.allUsers().subscribe(
        data2 => {
          this.users = data2.map(x => x.name);
          this.difference = _.difference(this.users, this.teamMembers);
        });
    });
  }

  addToTeam(name, team) {
    const act = {
      userName: name,
      teamName: team,
      action: 'add'
    };
    this.adminService.moveUser(act).subscribe();
    this.teamMembers = [name, ...this.teamMembers];
    this.difference = this.difference.filter(x => x !== name);
  }

  removeFromTeam(name, team) {
    const act = {
      userName: name,
      teamName: team,
      action: 'remove'
    };
    this.adminService.moveUser(act).subscribe();
    this.teamMembers = this.teamMembers.filter(x => x !== name);
    this.difference = [name, ...this.difference];
  }
}
