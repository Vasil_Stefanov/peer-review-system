import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-activity-log',
  templateUrl: './activity-log.component.html',
  styleUrls: ['./activity-log.component.css']
})
export class ActivityLogComponent implements OnInit {
  @Input()
  public activity;
  @Input()
  public loggedUser;
  constructor() { }

  ngOnInit() {}

}
