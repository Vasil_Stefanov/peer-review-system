import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-team-requests',
  templateUrl: './team-requests.component.html',
  styleUrls: ['./team-requests.component.css']
})
export class TeamRequestsComponent implements OnInit {
  @Input()
  public teamRequest;
  @Input()
  public loggedUser;
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
    switch (this.teamRequest.status) {
      case 0:
        this.teamRequest.status = 'Pending';
        break;
      case 1:
        this.teamRequest.status = 'Under Review';
        break;
      case 2:
        this.teamRequest.status = 'Change Requested';
        break;
      case 3:
        this.teamRequest.status = 'Accepted';
        break;
      case 4:
        this.teamRequest.status = 'Rejected';
        break;
      default:
        break;
    }
  }
}
