import { Component, OnInit, ElementRef, ViewChild, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';
import { AuthService } from '../../../app/core/services/auth.service';
import { TeamService } from '../../../app/core/services/team.service';
import { FormBuilder } from '@angular/forms';
import { RequestService } from '../../core/services/request.service';
import { SharedService } from '../../core/services/shared.service';
import { NotificationService } from '../../core/services/notification.service';
import { AdminService } from 'src/app/core/services/admin.service';

@Component({
  selector: 'app-review-request-list',
  templateUrl: './review-request-list.component.html',
  styleUrls: ['./review-request-list.component.css']
})
export class ReviewRequestListComponent implements OnInit, OnDestroy {

  public allUserRequests;
  public allTeamRequests;
  public allTeams;
  public activities;
  public allUsers;
  public isAdmin = false;
  public currentUserReviewRequests;
  public teamReviewRequests;
  public users;
  public added: string[] = [];
  public history;
  public interval;
  public loggedUser;
  public notifications;
  public p1 = 1;
  public p2 = 1;
  public p3 = 1;
  public p4 = 1;
  public p5 = 1;
  public p6 = 1;
  public p7 = 1;
  public p8 = 1;

  public userSubscription: Subscription;
  @ViewChild('tagInput') tagInputRef: ElementRef;
  public teams;
  public invites;
  constructor(
    public readonly activatedRoute: ActivatedRoute,
    public readonly authService: AuthService,
    public _elementRef: ElementRef,
    public fb: FormBuilder,
    public readonly teamService: TeamService,
    public requestService: RequestService,
    public readonly notificationService: NotificationService,
    public sharedService: SharedService,
    public adminService: AdminService

  ) {
    this.interval = setInterval(() => {
      this.getNotifications();
      if (this.notifications !== undefined) {
        this.sharedService.emitChange(this.notifications);
      }
    }, 2000);
  }

  ngOnInit() {
    this.userSubscription = this.authService.currentUser$.subscribe(res => {
      this.loggedUser = res;
    });
    this.activatedRoute.data.subscribe((data) => {
      if (this.loggedUser.roles === 'admin') {
        this.isAdmin = true;
        this.allUserRequests = data.request[0];
        this.allTeamRequests = data.request[1][0];
        this.allTeams = data.request[2];
        this.activities = data.request[3];
        this.allUsers = data.request[4].filter(x => x.roles !== 'admin');
      } else {
        console.log(data);
        this.currentUserReviewRequests = data.request[0].filter(x => x.isClosed !== true);
        if (data.request[1][0]) {
          this.teamReviewRequests = data.request[1][0].filter(x => x.isClosed !== true);
        }
        this.users = data.request[2].filter(x => x.name !== this.loggedUser.name);
        this.teams = data.request[3].teams;
        this.invites = data.request[3].invites;
        this.history = data.request[0].filter(x => x.isClosed === true);
      }
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  getNotifications() {
    this.notificationService.getNotifications().subscribe(
      data => {
        this.notifications = data;
      });
  }

  onTeamCreated(team) {
    this.teamService.createTeam(team).subscribe();
    this.teams = [team, ...this.teams];
  }

  onReviewRequestCreated(reviewRequest) {
    this.requestService.createReviewRequest(reviewRequest).subscribe();
    this.currentUserReviewRequests = [reviewRequest, ...this.currentUserReviewRequests];
    this.teamReviewRequests = [reviewRequest, ...this.teamReviewRequests];
  }

  onAssignedAdmin(name) {
    this.adminService.assignAsAdmin(name).subscribe();
    this.allUsers = this.allUsers.filter(x => x.name !== name);
  }
}
