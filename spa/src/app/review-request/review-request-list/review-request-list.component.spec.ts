import { async, ComponentFixture, TestBed, fakeAsync } from '@angular/core/testing';

import { ReviewRequestListComponent } from './review-request-list.component';
import { Router } from '@angular/router';
import { RequestService } from 'src/app/core/services/request.service';
import { AdminService } from 'src/app/core/services/admin.service';
import { SharedService } from 'src/app/core/services/shared.service';
import { NotificationService } from 'src/app/core/services/notification.service';
import { TeamService } from 'src/app/core/services/team.service';
import { AuthService } from 'src/app/core/services/auth.service';
import { routes } from '../review-request-routing.module';
import { SharedModule } from 'src/app/shared/shared.module';
import { RouterTestingModule } from '@angular/router/testing';
import { CoreModule } from 'src/app/core/core.module';
import { ReviewRequestDetailsComponent } from '../review-request-details/review-request-details.component';
import { RequestModalComponent } from '../request-modal/request-modal.component';
import { TeamModalComponent } from '../team-modal/team-modal.component';
import { AppModule } from 'src/app/app.module';
import { NgxPaginationModule } from 'ngx-pagination';
import { ReviewRequestUserViewComponent } from '../review-request-user/review-request-user-view.component';
import { TeamRequestsComponent } from '../admin-views/team-requests/team-requests.component';
import { ReviewRequestHistoryViewComponent } from '../review-request-history/review-request-history-view.component';
import { ActivityLogComponent } from '../admin-views/activity-log/activity-log.component';
import { UsersComponent } from '../admin-views/users/users.component';
import { UserRequestsComponent } from '../admin-views/user-requests/user-requests.component';
import { TeamsComponent } from '../admin-views/teams/teams.component';
import { ReviewRequestTeamViewComponent } from '../review-request-team/review-request-team-view.component';
import { of } from 'rxjs';

describe('ReviewRequestListComponent', () => {
  let component: ReviewRequestListComponent;
  let fixture;
  const teamService = jasmine.createSpyObj('TeamService', ['createTeam']);
  const notificationService = jasmine.createSpyObj('NotificationService', ['getNotifications']);
  const sharedService = jasmine.createSpyObj('SharedService', ['emitChange']);
  const adminService = jasmine.createSpyObj('AdminService', ['assignAsAdmin']);
  const requestService = jasmine.createSpyObj('RequestService', ['createReviewRequest']);

  const reviewRequest = {
    title: 'title',
    content: 'content',
    reviewers: 'reviewers',
    tags: 'tags',
    team: 'team',
    status: 1
  };

  const team = {
    name: 'team Name',
    users: ['ivan', 'pesho'],
  };

  const notification = {
    id: 'notificationId',
    content: 'notificationContent',
    user: 'ivan',
    key: 'request',
    checked: true,
  };
  const user = {
    id: 'userId',
    name: 'userName',
    password: 'userPassword',
    roles: 'admin',
    reviewRequest: [],
    teams: [],
    invites: [],
    reviews: [],
    voted: [],
    notification: [],
    ofTeam: [],
    createdOn: Date,
    updatedOn: Date
  };

  const user1 = {
    id: 'userId1',
    name: 'userName1',
    password: 'userPassword1',
    roles: 'admin',
    reviewRequest: [],
    teams: [],
    invites: [],
    reviews: [],
    voted: [],
    notification: [],
    ofTeam: [],
    createdOn: Date,
    updatedOn: Date
  };

  const request1 = {
    id: 'request1',
    title: 'titlerequest1',
    content: 'content',
    tags: 'user',
    assignee: 'review request1',
    team: 'team1',
    users: 'invite1',
    comments: false,
    voters: 'notification1',
    status: 'notification1',
    isClosed: 'notification1'

  };
  const request2 = {
    id: 'request2',
    title: 'titlerequest2',
    content: 'content',
    tags: 'user',
    assignee: 'review request2',
    team: 'team1',
    users: 'invite2',
    comments: false,
    voters: 'notification2',
    status: 'notification2',
    isClosed: 'notification2'
  };
  const requests = [request1, request2];
  const users = [user, user1];
  const authService = {
    currentUser$: of(user),
  };

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        ReviewRequestListComponent,
        ReviewRequestDetailsComponent,
        RequestModalComponent,
        TeamModalComponent,
        ReviewRequestUserViewComponent,
        TeamRequestsComponent,
        ReviewRequestHistoryViewComponent,
        ActivityLogComponent,
        UsersComponent,
        UserRequestsComponent,
        TeamsComponent,
        ReviewRequestTeamViewComponent,

      ],
      imports: [
        SharedModule,
        RouterTestingModule,
        AppModule,
        NgxPaginationModule,
        RouterTestingModule.withRoutes(routes),
        // PostModule,
        SharedModule,
      ],
      providers: [
        {
          provide: RequestService,
          useValue: requestService,
        },
        {
          provide: AdminService,
          useValue: adminService,
        },
        {
          provide: SharedService,
          useValue: sharedService,
        },
        {
          provide: NotificationService,
          useValue: notificationService,
        },
        {
          provide: TeamService,
          useValue: teamService,
        },
        {
          provide: AuthService,
          useValue: authService,
        },
      ],

    });

    fixture = TestBed.createComponent(ReviewRequestListComponent);
  });

  afterEach(() => {
    if (fixture.nativeElement && 'remove' in fixture.nativeElement) {
      (fixture.nativeElement as HTMLElement).remove();
    }
  });

  it('should create the app', () => {
    fixture = TestBed.createComponent(ReviewRequestListComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });



  describe('getNotifications', () => {
    it('should call the notificationService.getNotifications', async () => {
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.componentInstance;
      notificationService.getNotifications.calls.reset();
      notificationService.getNotifications.and.returnValue({ subscribe: () => { } });

      component.getNotifications();
      await fixture.detectChanges();
      expect(notificationService.getNotifications).toHaveBeenCalledTimes(1);
    });

    it('should return notifications', async () => {
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.componentInstance;
      notificationService.getNotifications.and.returnValue(of(notification));

      component.getNotifications();
      await fixture.detectChanges();

      notificationService.getNotifications().subscribe(
        res => {
          expect(res).toBe(notification);
        });
    });
  });

  describe('onTeamCreated', () => {
    it('should call the teamService.createTeam', () => {
      fakeAsync(() => {
        fixture = TestBed.createComponent(ReviewRequestListComponent);
        component = fixture.debugElement.componentInstance;
        teamService.createTeam.and.returnValue(of(team));
        teamService.createTeam.calls.reset();

        component.onTeamCreated(team);
        fixture.detectChanges();
        expect(teamService.createTeam).toHaveBeenCalledTimes(1);
      });
    });
  });

  describe('onReviewRequestCreated', () => {
    it('should call the requestService.createReviewRequest', async () => {
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.debugElement.componentInstance;
      component.allUserRequests = requests;
      component.allTeamRequests = requests;
      requestService.createReviewRequest.calls.reset();
      requestService.createReviewRequest.and.returnValue(of(reviewRequest));

      await fixture.detectChanges();
      requestService.createReviewRequest(reviewRequest).subscribe(
        () => {
          const newUserRequests = [reviewRequest, ...component.allUserRequests];
          expect(newUserRequests).toEqual([reviewRequest, ...component.allUserRequests]);
        });
      expect(requestService.createReviewRequest).toHaveBeenCalledTimes(1);
    });
  });

  describe('onAssignedAdmin', () => {
    it('should call the adminService.assignAsAdmin', async () => {
      authService.currentUser$ = of(user);
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.debugElement.componentInstance;
      component.allUsers = users;
      adminService.assignAsAdmin.calls.reset();
      adminService.assignAsAdmin.and.returnValue(of(user));

      adminService.assignAsAdmin(user.name);
      await fixture.detectChanges();
      expect(adminService.assignAsAdmin).toHaveBeenCalledTimes(1);
    });

    it('should remove user from allUsers', async () => {
      authService.currentUser$ = of(user);
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.componentInstance;
      component.allUsers = users;
      adminService.assignAsAdmin.calls.reset();
      adminService.assignAsAdmin.and.returnValue(of([user1]));

      adminService.assignAsAdmin(user.name).subscribe(
        async (res) => {
          const newAllUsers = component.allUsers.filter((u) => u.name !== user.name);
          await fixture.detectChanges();
          expect(res[0].name).toEqual(user1.name);
        },
      );
    });
  });

  describe('ngOnInit', () => {
    it('should initialize proper user', async () => {
      fixture = TestBed.createComponent(ReviewRequestListComponent);
      component = fixture.componentInstance;
      await fixture.detectChanges();
      expect(component.loggedUser).toBe(user);
    });
  });
});
