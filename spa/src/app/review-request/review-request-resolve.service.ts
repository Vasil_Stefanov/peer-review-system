import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { NotificatorService } from '../core/services/notificator.service';
import { of, forkJoin, Subscription } from 'rxjs';
import { RequestService } from '../core/services/request.service';
import { UsersService } from '../core/services/users.service';
import { TeamService } from '../core/services/team.service';
import { AuthService } from '../core/services/auth.service';
import { AdminService } from '../core/services/admin.service';

@Injectable({
    providedIn: 'root'
})
export class RequestResolveService implements Resolve<any[]> {
    private role;
    private userSubscription: Subscription;
    constructor(
        private readonly requestService: RequestService,
        private readonly notificator: NotificatorService,
        private readonly userService: UsersService,
        private readonly teamService: TeamService,
        private readonly authService: AuthService,
        private readonly adminService: AdminService
    ) { }

    public resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ) {
        this.userSubscription = this.authService.currentUser$.subscribe(res => {
            this.role = res.roles;
        });
        const userItems = [
            this.requestService.allLoggedUserRequests(),
            this.requestService.getTeamRequests(),
            this.userService.allUsers(),
            this.teamService.getTeams(),
        ];
        const adminItems = [
            this.adminService.getAllUserRequests(),
            this.adminService.getAllTeamRequests(),
            this.adminService.getAllTeams(),
            this.adminService.getActivity(),
            this.adminService.getAllUsers()
        ];
        if (this.role === 'admin') {
            return forkJoin(adminItems);
        } else {
            return forkJoin(userItems);
        }
    }
}
