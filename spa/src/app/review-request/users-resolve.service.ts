// import { Injectable } from '@angular/core';
// import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
// import { NotificatorService } from '../core/services/notificator.service';
// import { catchError } from 'rxjs/operators';
// import { of } from 'rxjs';
// import { RequestService } from '../core/services/request.service';
// import { AuthService } from '../core/services/auth.service';
// import { UsersService } from '../core/services/users.service';

// @Injectable({
//     providedIn: 'root'
// })
// export class UsersResolveService implements Resolve<any> {

//     constructor(
//         private readonly usersService: UsersService,
//         private readonly notificator: NotificatorService,
//     ) {}

//     public resolve(
//         route: ActivatedRouteSnapshot,
//         state: RouterStateSnapshot,
//     ) {
//         return this.usersService.allUsers()
//         .pipe(catchError(
//             res => {
//                     this.notificator.error(res.error.error);
//                 return of([]);
//             }
//         ));
//     }
// }
