import { Routes, RouterModule } from '@angular/router';
import { NgModule } from '@angular/core';
import { AuthGuard } from '../auth/auth.guard';
import { ReviewRequestListComponent } from './review-request-list/review-request-list.component';
import { ReviewRequestDetailsComponent } from './review-request-details/review-request-details.component';
import { RequestResolveService } from './review-request-resolve.service';

export const routes: Routes = [
    { path: '', component: ReviewRequestListComponent, pathMatch: 'full',
     canActivate: [AuthGuard], resolve: { request: RequestResolveService } },
    { path: ':id', component: ReviewRequestDetailsComponent, canActivate: [AuthGuard] },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class ReviewRequestRoutingModule { }
