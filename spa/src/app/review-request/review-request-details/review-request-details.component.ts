import { Component, OnInit } from '@angular/core';
import { RequestService } from 'src/app/core/services/request.service';
import { ActivatedRoute } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/core/services/auth.service';
import { CommentsService } from 'src/app/core/services/comments.service';
import { AdminService } from 'src/app/core/services/admin.service';

@Component({
  selector: 'app-review-request-details',
  templateUrl: './review-request-details.component.html',
  styleUrls: ['./review-request-details.component.css']
})
export class ReviewRequestDetailsComponent implements OnInit {
  form: FormGroup;
  public specificRequest: any;
  public comments = [];
  private createCommentForm: FormGroup;
  private userSubscription: Subscription;
  public loggedUser;
  public isOwner: boolean;
  public tags = [];
  public hasVoted: boolean;
  public message = 'Vote';
  public isAdmin: boolean;
  constructor(
    private readonly requestService: RequestService,
    private readonly route: ActivatedRoute,
    private readonly formBuilder: FormBuilder,
    private readonly authService: AuthService,
    private readonly commentsService: CommentsService,
    private fb: FormBuilder,
    private adminService: AdminService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      tag: [undefined],
    });
    this.userSubscription = this.authService.currentUser$.subscribe(res => {
      this.loggedUser = res;
    });
    if (this.loggedUser.roles === 'admin') {
      this.isAdmin = true;
    }
    const name = this.route.snapshot.params.id;
    this.requestService.getReviewRequestByTitle(name).subscribe(
      data => {
        this.specificRequest = data.request;

        if (data.voters.find(x => x.name === this.loggedUser.name)) {
          this.message = 'You\'ve already voted';

          this.hasVoted = true;
        }
        if (data.request.author === this.loggedUser.name) {
          this.isOwner = true;
        } else {
          this.isOwner = false;
          if (this.specificRequest.status === 0) {
            this.updateStatus('review', this.specificRequest.id);
            this.specificRequest.status = 1;
          }
        }
        switch (this.specificRequest.status) {
          case 0:
            this.specificRequest.status = 'Pending';
            break;
          case 1:
            this.specificRequest.status = 'Under Review';
            break;
          case 2:
            this.specificRequest.status = 'Change Requested';
            break;
          case 3:
            this.specificRequest.status = 'Accepted';
            break;
          case 4:
            this.specificRequest.status = 'Rejected';
            break;
          default:
            break;
        }
        this.tags = data.request.tags.split(',');
        this.commentsService.getPostComments(this.specificRequest.title).subscribe(
          comments => {
            this.comments = comments;
          });
      });
    this.createCommentForm = this.formBuilder.group({
      content: ['', [Validators.required]]
    });

    this.createCommentForm.setValue({ content: this.comments });
  }

  updateStatus(vote, id) {
    const passVote = {
      id: id,
      status: vote
    };
    this.requestService.updateStatus(passVote).subscribe();
  }

  addComment(title) {
    const content = this.createCommentForm.value.content;
    const newComment = {
      title: title,
      author: this.loggedUser.name,
      content: content
    };
    this.comments = [newComment, ...this.comments];
    this.commentsService.createComments(newComment).subscribe();
  }

  closeRequest(id, act) {
    const action = {
      id: id,
      act: act
    };
    this.adminService.manipulateRequest(action).subscribe();
    this.specificRequest.isClosed = true;
    if (act === 'accepted') {
      this.specificRequest.status = 'Accepted';
    } else {
      this.specificRequest.status = 'Rejected';
    }
  }
}
