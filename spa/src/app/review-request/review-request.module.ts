import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ReviewRequestDetailsComponent } from './review-request-details/review-request-details.component';
import { ReviewRequestListComponent } from './review-request-list/review-request-list.component';
import { ReviewRequestRoutingModule } from './review-request-routing.module';
import { TeamModalComponent } from './team-modal/team-modal.component';
import { RequestModalComponent } from './request-modal/request-modal.component';
import { ReviewRequestUserViewComponent } from './review-request-user/review-request-user-view.component';
import { ReviewRequestTeamViewComponent } from './review-request-team/review-request-team-view.component';
import { ReviewRequestHistoryViewComponent } from './review-request-history/review-request-history-view.component';
import { CommentsComponent } from './review-request-details/comments/comments.component';
import { UserRequestsComponent } from './admin-views/user-requests/user-requests.component';
import { TeamRequestsComponent } from './admin-views/team-requests/team-requests.component';
import { TeamsComponent } from './admin-views/teams/teams.component';
import { ActivityLogComponent } from './admin-views/activity-log/activity-log.component';
import { UsersComponent } from './admin-views/users/users.component';
import { NgxPaginationModule } from 'ngx-pagination';


@NgModule({
  declarations: [
    ReviewRequestListComponent,
    ReviewRequestDetailsComponent,
    TeamModalComponent,
    RequestModalComponent,
    ReviewRequestUserViewComponent,
    ReviewRequestTeamViewComponent,
    ReviewRequestHistoryViewComponent,
    CommentsComponent,
    UserRequestsComponent,
    TeamRequestsComponent,
    TeamsComponent,
    ActivityLogComponent,
    UsersComponent,
  ],
  entryComponents: [],
  imports: [
    SharedModule, FormsModule, ReviewRequestRoutingModule, NgxPaginationModule
  ],
  providers: [],
})
export class ReviewRequestModule { }
