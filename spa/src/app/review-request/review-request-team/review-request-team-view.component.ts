import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-review-request-team-view',
  templateUrl: './review-request-team-view.component.html',
  styleUrls: ['./review-request-team-view.component.css']
})
export class ReviewRequestTeamViewComponent implements OnInit {
  @Input()
  public teamRequest;
  @Input()
  public loggedUser;
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
    switch (this.teamRequest.status) {
      case 0:
      this.teamRequest.status = 'Pending';
        break;
      case 1:
        this.teamRequest.status = 'Under Review';

        break;
      case 2:
        this.teamRequest.status = 'Change Requested';

        break;
      default:
        break;
    }
  }

}
