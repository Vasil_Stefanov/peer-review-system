import { Component, OnInit, ElementRef, ViewChild, Input, Output, EventEmitter } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { find, get, pull } from 'lodash';
import { TeamService } from 'src/app/core/services/team.service';
import { RequestService } from 'src/app/core/services/request.service';

@Component({
  selector: 'app-request-modal',
  templateUrl: './request-modal.component.html',
  styleUrls: ['./request-modal.component.css']
})
export class RequestModalComponent implements OnInit {
  @ViewChild('tagInput') tagInputRef: ElementRef;
  form: FormGroup;
  isTeamSelected = true;
  reviewRequestForm: FormGroup;
  tags: string[] = [];
  private users;
  @Input()
  private teams;
  @Input()
  private loggedUser;
  private teamSelected;
  private usersSelected: string[] = [];
  @Output()
  private requestCreated = new EventEmitter();
  constructor(
    private readonly formBuilder: FormBuilder,
    private fb: FormBuilder,
    private elementRef: ElementRef,
    private teamService: TeamService,
  ) { }

  ngOnInit() {
    this.reviewRequestForm = this.formBuilder.group({
      title: ['', [Validators.required]],
      content: ['', [Validators.required]],
      tag: ['', [Validators.required]],
      checkTeam: ['', [Validators.required]],
      checkUser: ['', [Validators.required]],

    });
    this.form = this.fb.group({
      tag: [undefined],
    });
  }
  focusTagInput(): void {
    this.tagInputRef.nativeElement.focus();
  }
  onKeyUp(event: KeyboardEvent): void {
    const inputValue: string = this.form.controls.tag.value;
    if (event.code === 'Backspace' && !inputValue) {
      this.removeTag();
      return;
    } else {
      if (event.code === 'Comma' || event.code === 'Space') {
        this.addTag(inputValue);
        this.form.controls.tag.setValue('');
      }
    }
  }

  addTag(tag: string): void {
    if (tag[tag.length - 1] === ',' || tag[tag.length - 1] === ' ') {
      tag = tag.slice(0, -1);
    }
    if (tag.length > 0 && !find(this.tags, tag)) {
      this.tags.push(tag);
    }
  }

  removeTag(tag?: string): void {
    if (!!tag) {
      pull(this.tags, tag);
    } else {
      this.tags.splice(-1);
    }
  }

  createReviewRequest(value) {
    const reviewRequest = {
      title: value.title,
      content: value.content,
      reviewers: this.usersSelected,
      tags: this.tags,
      team: this.teamSelected,
      status: 0
    };
    this.requestCreated.emit(reviewRequest);
  }

  getUsers(value) {
    this.elementRef.nativeElement.querySelector(`#dropdownMenuButton`).innerText = value;
    this.teamService.teamMembers(value).subscribe(res => {
      this.users = res.filter(x => x.name !== this.loggedUser.name);
      this.isTeamSelected = false;
      this.teamSelected = value;
      this.elementRef.nativeElement.querySelector(`#userButton`).disabled = this.isTeamSelected;
    });
  }

  selectUsers(name) {
    const checj = this.elementRef.nativeElement.querySelector(`#${name}`);

    if (this.usersSelected.includes(name)) {
      this.usersSelected = this.usersSelected.filter(x => x !== name);
      checj.checked = false;
    } else {
      this.usersSelected = [name, ...this.usersSelected];
    }
  }
}
