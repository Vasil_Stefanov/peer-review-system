import { Component, OnInit, Input, ElementRef, Output, EventEmitter } from '@angular/core';
import { Team } from 'src/app/common/models/team';
import { TeamService } from 'src/app/core/services/team.service';

@Component({
  selector: 'app-team-modal',
  templateUrl: './team-modal.component.html',
  styleUrls: ['./team-modal.component.css']
})
export class TeamModalComponent implements OnInit {
  @Input()
  users;
  private added: string[] = [];
  @Output()
  private teamCreated = new EventEmitter();
  constructor(
    private _elementRef: ElementRef,


  ) { }

  ngOnInit() {
  }
  selected(name) {
    const checj = this._elementRef.nativeElement.querySelector(`#${name}`);
    if (this.added.includes(name)) {
      this.added = this.added.filter(x => x !== name);
      checj.checked = false;
    } else {
      this.added = [name, ...this.added];
    }
  }

  createTeam(name) {
    if (name.length > 0) {
      const team: Team = {
        name: name,
        users: this.added
      };
      this.teamCreated.emit(team);
    }
  }
}
