import { Component, OnInit, Input, ElementRef } from '@angular/core';

@Component({
  selector: 'app-review-request-history-view',
  templateUrl: './review-request-history-view.component.html',
  styleUrls: ['./review-request-history-view.component.css']
})
export class ReviewRequestHistoryViewComponent implements OnInit {
  @Input()
  public historyRequest;
  @Input()
  public loggedUser;
  constructor(
    private _elementRef: ElementRef,

  ) { }

  ngOnInit() {
    switch (this.historyRequest.status) {
      case 0:
      this.historyRequest.status = 'Pending';
        break;
      case 1:
        this.historyRequest.status = 'Under Review';

        break;
      case 2:
        this.historyRequest.status = 'Change Requested';

        break;
      default:
        break;
    }
  }

}
