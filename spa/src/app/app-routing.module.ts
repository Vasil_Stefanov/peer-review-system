import { SignInComponent } from './auth/signIn/signIn.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
  { path: 'signIn', component: SignInComponent },
  { path: 'requests' , loadChildren: './review-request/review-request.module#ReviewRequestModule'},
  { path: 'profile', loadChildren: './profile/profile.module#ProfileModule' },
  // { path: 'admin', loadChildren: './admin/admin.module#AdminModule' },
  // { path: 'comments', loadChildren: './comments/comments.module#CommentsModule' },
  // { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
