import { NgModule } from '@angular/core';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ProfileRoutingModule } from './profile-routing.mmodule';
import { ProfileComponent } from './profile.component';
import { ProfileTeamsComponent } from './profile-teams/profile-teams.component';
import { ProfileInvitesComponent } from './profile-invites/profile-invites.component';

@NgModule({
  declarations: [
      ProfileComponent,
      ProfileTeamsComponent,
      ProfileInvitesComponent
  ],
  entryComponents: [],
  imports: [
    SharedModule, FormsModule, ProfileRoutingModule
  ],
  providers: [],
})
export class ProfileModule { }
