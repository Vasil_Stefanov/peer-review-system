import { Injectable } from '@angular/core';
import { Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { NotificatorService } from '../core/services/notificator.service';
import { catchError } from 'rxjs/operators';
import { of, forkJoin, Observable } from 'rxjs';
import { RequestService } from '../core/services/request.service';
import { UsersService } from '../core/services/users.service';
import { TeamService } from '../core/services/team.service';

@Injectable({
    providedIn: 'root'
})
export class ProfileResolveService implements Resolve<any[]> {

    constructor(
        private readonly requestService: RequestService,
        private readonly notificator: NotificatorService,
        private readonly userService: UsersService,
        private readonly teamService: TeamService,
    ) { }

    public resolve(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ) {
        return forkJoin([
            this.requestService.allLoggedUserRequests(),
            this.teamService.getTeams(),
        ]);
    }
}
