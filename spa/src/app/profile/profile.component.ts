import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { AuthService } from '../core/services/auth.service';
import { SharedService } from '../core/services/shared.service';
import { NotificationService } from '../core/services/notification.service';
import { TeamService } from 'src/app/core/services/team.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private currentUserReviewRequests;
  private teams;
  private invites;
  public loggedUser;
  public notifications;
  private interval;
  private userSubscription: Subscription;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
    private readonly authService: AuthService,
    private sharedService: SharedService,
    private readonly notificationService: NotificationService,
    private readonly teamService: TeamService
  ) {
    this.interval = setInterval(() => {
      this.getNotifications();
      if (this.notifications !== undefined) {
        this.sharedService.emitChange(this.notifications);
      }
    }, 2000);
  }

  ngOnInit() {
    this.userSubscription = this.authService.currentUser$.subscribe(res => {
      this.loggedUser = res;
    });
    this.activatedRoute.data.subscribe((data) => {
      this.currentUserReviewRequests = data.profile[0];
      this.invites = [...data.profile[1].invites.filter(x => x.status === 0)];
      this.teams = [...data.profile[1].teams];
    });
  }

  ngOnDestroy(): void {
    clearInterval(this.interval);
  }

  getNotifications() {
    this.notificationService.getNotifications().subscribe(
      data => {
      this.notifications = data;
      });
  }

  onInviteResolved(response) {
    this.teamService.resolveInvite(response).subscribe();
    setTimeout(() => {
      this.teamService.getTeams().subscribe(
        data => {
          this.teams = data.teams;
          this.invites = [...data.invites];
        });
    }, 100);
  }

  onLeavingTeam(id) {
    this.teamService.leaveTeam(id).subscribe(
    );
    this.teams = this.teams.filter(x => x.id !== id);
  }
}
