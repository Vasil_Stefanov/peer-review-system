import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-profile-teams',
  templateUrl: './profile-teams.component.html',
  styleUrls: ['./profile-teams.component.css']
})
export class ProfileTeamsComponent implements OnInit {
  @Input()
  public team;
  @Input()
  public loggedUser;
  @Output()
  public teamLeft = new EventEmitter();
  constructor() { }

  ngOnInit() {}
  leave(id) {
    this.teamLeft.emit(id);
  }
}
