import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-profile-invites',
  templateUrl: './profile-invites.component.html',
  styleUrls: ['./profile-invites.component.css']
})
export class ProfileInvitesComponent implements OnInit {
  @Input()
  public invite;
  @Input()
  public loggedUser;
  @Output()
  public inviteResolved = new EventEmitter();
  constructor(
  ) { }

  ngOnInit() {
  }

  respond(response, id, team) {
    const responseObj = {
      id: id,
      status: response,
      teamName: team
    };
    this.inviteResolved.emit(responseObj);
  }
}
