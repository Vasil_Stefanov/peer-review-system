import { AuthGuard } from './../auth/auth.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProfileComponent } from './profile.component';
import { ProfileResolveService } from './profile-resolve.service';

const routes: Routes = [
  { path: '', redirectTo: ':username', pathMatch: 'full' },
  { path: ':username', component: ProfileComponent, canActivate: [AuthGuard] , resolve: {profile: ProfileResolveService},
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
